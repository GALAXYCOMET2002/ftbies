function DrawGraph() {
    var charts = {};
    charts.generation = new Chart(document.getElementById("chartallgen").getContext("2d"), {
        type: "doughnut",
        data: {
            labels: ["Red", "Green", "Yellow"],
            datasets: [{
                backgroundColor: ["#e74c3c","#2ecc71","#3498db"],
                hoverBackgroundColor: ["#c0392b","#27ae60","#2980b9"]
            }],
            data: [300,50,200]
        },options: {
            //アニメーションの設定
            animation: {
                //アニメーションの有無
                animateRotate: true
            }
        }
    });
    charts.generation.update();
    console.log(charts.generation);
}

$(DrawGraph);